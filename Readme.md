Snail Rally
===========

This is a little game for Homeputerium's Basic Tenliner 2016 contest [http://gkanold.wix.com/homeputerium#!basic-tenliners-2016/c450]

##Instructions
Join to this snail in a race, helping to dodge rocks the way. Beware that the road gets more stony.

##Controls##
Cursor keys up and down or joystick into port 1 up and down, to move the snail in that address (up and down only).
Space key or joystick button 1 to jump.

##Files in repository

 * SnailRally.bas : Basic program
 * Snail Rally.dsk : Disk image for executing in emulators.